// Константы

const video = document.querySelector('video');
const container_video = document.querySelector('.container_video');
const dict = document.querySelector('.dict');
const word = document.querySelectorAll('.word');
const play = document.querySelector('.play');
const pause = document.querySelector('.pause');
const round_cnt = document.querySelector('.round_cnt');
const body = document.querySelector('body');

let dict_from = '';

let sub = document.createElement('div');
sub.setAttribute('id', 'sub');

let sub_first = document.createElement('div');
sub_first.setAttribute('class', 'sub_first');

let sub_second = document.createElement('div');
sub_second.setAttribute('class', 'sub_second');

sub.appendChild(sub_first);
sub.appendChild(sub_second);

let trans = document.createElement('div');
trans.setAttribute('class', 'trans');

let exact_trans = document.createElement('div');
exact_trans.setAttribute('class', 'exact-trans');

let exact_from = document.createElement('div');
exact_from.setAttribute('class', 'exact-from');

let span = document.createElement('span');
span.innerText = ' ' + '-' + ' ';

let exact_to = document.createElement('div');
exact_to.setAttribute('class', 'exact-to');

let trans_wrapper = document.createElement('div');
trans_wrapper.setAttribute('class', 'trans-wrapper');

let is = document.createElement('i');
is.setAttribute('class', 'fa fa-caret-right fa-2x');

let close_wrap = document.createElement('div');
close_wrap.setAttribute('class', 'close-wrap');


trans.appendChild(exact_trans);
exact_trans.appendChild(exact_from);
exact_trans.appendChild(span);
exact_trans.appendChild(exact_to);
trans.appendChild(trans_wrapper);
trans.appendChild(is);

let english_sub = document.querySelector('.english_sub');

    english_sub.addEventListener('click', function () {
        english_sub.classList.toggle('sub_active');
        if (!(english_sub.classList.contains('sub_active'))) {
            sub_first.innerText = '';
            sub_second.innerText = '';
        }
    })





setTimeout(function() {preloader.style.display = 'none';}, 8000);

let swith = document.querySelectorAll('.sub_switch');
let russian_sub = document.querySelector('.russian_sub');


    english_sub.addEventListener('click', function () {
        if (!(english_sub.classList.contains('sub_active'))) {
            english_sub.classList.add('sub_active');
            russian_sub.classList.remove('sub_active');
        } else {
            russian_sub.classList.remove('sub_active');
        }
    })

    russian_sub.addEventListener('click', function () {
        if (!(russian_sub.classList.contains('sub_active'))) {
            russian_sub.classList.add('sub_active');
            english_sub.classList.remove('sub_active');
        }
    })


for (i of swith) {
    i.addEventListener('click', function () {
        sub_first.innerHTML = '';
        sub_second.innerHTML = '';

        this.classList.add('sub_active');
    })
}

// Копирование при клике







// Запрашиваем перевод 

let yatr_word = {

    key: 'trnsl.1.1.20170627T100903Z.3134f17b23b61deb.2a264dc99360357e7cc00655d8e203586280a98f',
    api: 'https://translate.yandex.net/api/v1.5/tr.json/translate',

    translate: function (text) {
        var url = this.api + '?';

        url += 'key=' + this.key;
        url += '&text=' + text;
        url += '&lang=en-ru';

        let ajax = new XMLHttpRequest();
        ajax.open('GET', url, true);
        ajax.onreadystatechange = function () {
            if (ajax.readyState == 4) {
                if (ajax.status == 200) {
                    let translate = ajax.responseText;
                    translate = JSON.parse(translate);
                    translate = translate.text[0];
                    exact_from.innerText = text;
                    exact_to.innerText = translate;
                }
            }
        }
        ajax.send(null);
    }
};

let yatr = {

    key: 'dict.1.1.20170701T173958Z.b33374ebce561506.4d2ffc0f1cedd96f29117c84fd4d0e8145c803cd',
    api: 'https://dictionary.yandex.net/api/v1/dicservice.json/lookup',

    translate: function (text) {
        var url = this.api + '?';

        url += 'key=' + this.key;
        url += '&text=' + text;
        url += '&lang=en-ru';

        var ajax = new XMLHttpRequest();
        ajax.open('GET', url, true);
        ajax.onreadystatechange = function () {
            if (ajax.readyState == 4) {
                if (ajax.status == 200) {
                    let translate = ajax.responseText;
                    translate = JSON.parse(translate);
                    translate = translate.def;
                    for (i of translate) {
                        switch (i.pos) {
                            case 'noun':
                                let cnt_noun = 0;
                                for (m of i.tr) {
                                    if (cnt_noun > 2) {
                                        break;
                                    } else {
                                        cnt_noun++;
                                        translate = m.text;
                                        if (exact_to.innerText === translate) {
                                            break;
                                        }
                                        let noun = document.createElement('div');
                                        noun.setAttribute('class', 'word-item');
                                        noun.innerText = translate;
                                        trans_wrapper.appendChild(noun);
                                    }

                                }
                                break;
                            case 'interjection':
                                let cnt_int = 0;
                                for (m of i.tr) {
                                    if (cnt_int > 2) {
                                        break;
                                    } else {
                                        cnt_int++;
                                        translate = m.text;
                                        if (exact_to.innerText === translate) {
                                            break;
                                        }
                                        let int = document.createElement('div');
                                        int.setAttribute('class', 'word-item');
                                        int.innerText = translate;
                                        trans_wrapper.appendChild(int);
                                    }

                                }
                                break;
                            case 'verb':
                                let cnt_verb = 0;
                                for (m of i.tr) {
                                    if (cnt_verb > 2) {
                                        break;
                                    } else {
                                        cnt_verb++;
                                        translate = m.text;
                                        if (exact_to.innerText === translate) {
                                            break;
                                        }
                                        let verb = document.createElement('div');
                                        verb.setAttribute('class', 'word-item');
                                        verb.innerText = translate;
                                        trans_wrapper.appendChild(verb);
                                    }
                                }
                                break;
                            case 'pronoun':
                                let cnt_pronoun = 0;
                                for (m of i.tr) {
                                    if (cnt_pronoun > 2) {
                                        break;
                                    }
                                    cnt_pronoun++;
                                    translate = m.text;
                                    if (exact_to.innerText === translate) {
                                        break;
                                    }
                                    let pronoun = document.createElement('div');
                                    pronoun.setAttribute('class', 'word-item');
                                    pronoun.innerText = translate;
                                    trans_wrapper.appendChild(pronoun);
                                }
                                break;
                            case 'adjective':
                                let cnt_adj = 0;
                                for (m of i.tr) {
                                    if (cnt_adj > 2) {
                                        break;
                                    }
                                    cnt_adj++;
                                    translate = m.text;
                                    if (exact_to.innerText === translate) {
                                        break;
                                    }
                                    let adjective = document.createElement('div');
                                    adjective.setAttribute('class', 'word-item');
                                    adjective.innerText = translate;
                                    trans_wrapper.appendChild(adjective);
                                }
                                break;
                            case 'adverb':
                                let cnt_adverb = 0;
                                for (m of i.tr) {
                                    if (cnt_adverb > 2) {
                                        break;
                                    }
                                    cnt_adverb++;
                                    translate = m.text;
                                    if (exact_to.innerText === translate) {
                                        break;
                                    }
                                    let adverb = document.createElement('div');
                                    adverb.setAttribute('class', 'word-item');
                                    adverb.innerText = translate;
                                    trans_wrapper.appendChild(adverb);
                                }
                                break;
                            case 'preposition':
                                let cnt_prepos = 0;
                                for (m of i.tr) {
                                    if (cnt_prepos > 2) {
                                        break;
                                    }
                                    cnt_prepos++;
                                    translate = m.text;
                                    if (exact_to.innerText === translate) {
                                        break;
                                    }
                                    let prepos = document.createElement('div');
                                    prepos.setAttribute('class', 'word-item');
                                    prepos.innerText = translate;
                                    trans_wrapper.appendChild(prepos);
                                }
                                break;
                            default:
                                break;
                        }
                    }
                    show_card();
                }
            }
        }
        ajax.send(null);
    },

};

// Показываем варианты

trans.addEventListener('mouseover', function () {

    let exact_from = document.querySelector('.exact-from');

    exact_trans.onclick = function () {

        let exact_to = document.querySelector('.exact-to');

        this.style.color = 'grey';
        log(exact_from.innerText, exact_to.innerText)
    };

    let word_item = document.querySelectorAll('.word-item');

    for (i of word_item) {
        i.onclick = function () {
            this.style.color = 'grey';
            log(exact_from.innerText, this.innerText);
        }
    }

})

let cnt_dic = 0;
let all_words_in_dic = [];
let dict_wrapper_container = [];

function log(from, to) {

    let dict_from = '';

    let dict_wrapper = document.createElement('div');
    dict_wrapper.setAttribute('class', 'dict_wrapper');
    dict_wrapper.style.display = 'flex';

    let tire = document.createElement('span');
    tire.innerText = ' ' + ' - ' + ' ';

    dict_from = document.createElement('div');
    dict_from.setAttribute('class', 'dict_from');
    dict_from.innerText += (from);

    let dict_to = document.createElement('div');
    dict_to.setAttribute('class', 'dict_to');
    dict_to.innerText += (to);

    all_words_in_dic.push(dict_from);

    dict_wrapper.appendChild(dict_from);
    dict_wrapper.appendChild(tire);
    dict_wrapper.appendChild(dict_to);

    dict.appendChild(dict_wrapper)
    
    dict_wrapper_container.push(dict_wrapper);

    let cnt = '';

    cnt_dic++;

    round_cnt.innerText = cnt_dic;

    if (!(round_cnt.classList.contains('lime_cnt'))) {
        round_cnt.classList.add('lime_cnt')
    }

};

// Добавляем в словарь

let coordinats_left = '';
let coordinats_top = '';

sub.addEventListener('click', function (e) {
    let word = document.querySelectorAll('.word');

    trans_wrapper.innerText = " ";
    exact_trans.style.color = 'black';

    for (i = 0; i < word.length; i++) {
        word[i].onclick = function () {

            let dictation = '';
            dictation = this.innerText.toLowerCase();
            coordinats_left = '';
            coordinats_top = '';
            let mix = offset(this);
            coordinats_left = Math.round(mix.left);
            coordinats_top = Math.round(mix.top);

            yatr_word.translate(dictation);
            yatr.translate(dictation);
        }
    }

}, true)

var containers = '';

sub.addEventListener('contextmenu', function (e) {
    let word = document.querySelectorAll('.word');
    e.preventDefault();
    for (i = 0; i < word.length; i++) {

        let dictation = '';
        dictation = this.innerText.toLowerCase();

        var containers;
        word[i].oncontextmenu = function (containers) {
            containers = this.innerText;

            coordinats_left = '';
            coordinats_top = '';
            let mix = offset(this);
            coordinats_left = Math.round(mix.left);
            coordinats_top = Math.round(mix.top);

            show_card();

            if (!(this.classList.contains('chosed'))) {
                this.classList.add('chosed');
                num(containers)
            }

        }
    }


}, true)

function offset(el) {
    var rect = el.getBoundingClientRect(),
        scrollLeft = window.pageXOffset || document.documentElement.scrollLeft,
        scrollTop = window.pageYOffset || document.documentElement.scrollTop;
    return {
        top: rect.top + scrollTop,
        left: rect.left + scrollLeft
    }
}

let con = '';

function num(text) {
    con += (text.toLowerCase() + ' ');
    yatr_word.translate(con);

    let word = document.querySelectorAll('.word');

    close_wrap.addEventListener('contextmenu', function (e) {
        e.preventDefault();
        this.classList.remove('show');
        trans.classList.remove('show');
        con = ''
        for (i of word) {
            i.classList.remove('chosed');
            if (!(exact_trans)) {
                for (m of exact_trans) {
                    m.style.color = 'black';
                }
            }
        }
    })

    close_wrap.addEventListener('click', function (e) {
        con = '';
        for (i of word) {
            i.classList.remove('chosed');
            if (!(exact_trans)) {
                for (m of exact_trans) {
                    m.style.color = 'black';
                }
            }
        }
    })

}

function show_card() {

    trans.classList.add('show');
    close_wrap.classList.add('show');

    let trans_height = trans.offsetHeight;

    trans.style.left = (coordinats_left - 35) + 'px';
    trans.style.top = (coordinats_top - (trans_height + 48)) + 'px';
}

close_wrap.addEventListener('click', function () {

    this.classList.remove('show');
    trans.classList.remove('show');
    trans_wrapper.innerText = " ";

});

// Как использовать

let use_span = document.querySelectorAll('.use_span');
let use_now = document.querySelector('.use_now');
let use_late = document.querySelector('.use_late');
if (use_late) {
    use_late.innerHTML = use_span.length;
}

let use_span__cnt = 1;

function how_to_use__right(e) {

    if (use_span__cnt == use_span.length) {
        return false;
    } else {
        use_span[use_span__cnt - 1].classList.remove('use_span__visible');
    }

    use_span__cnt++;
    use_now.innerHTML = use_span__cnt;

    use_span[use_span__cnt - 1].classList.add('use_span__visible');

}

function how_to_use__left() {

    if (use_span__cnt == 1) {
        return false;
    }

    use_span[use_span__cnt - 1].classList.remove('use_span__visible');

    use_span__cnt--;
    use_now.innerHTML = use_span__cnt;

    use_span[use_span__cnt - 1].classList.add('use_span__visible');

}

let preloader = document.querySelector('.preloader');
