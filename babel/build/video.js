'use strict';

// Константы

var video = document.querySelector('video');
var container_video = document.querySelector('.container_video');
var dict = document.querySelector('.dict');
var word = document.querySelectorAll('.word');
var play = document.querySelector('.play');
var pause = document.querySelector('.pause');
var round_cnt = document.querySelector('.round_cnt');
var body = document.querySelector('body');

var dict_from = '';

var sub = document.createElement('div');
sub.setAttribute('id', 'sub');

var sub_first = document.createElement('div');
sub_first.setAttribute('class', 'sub_first');

var sub_second = document.createElement('div');
sub_second.setAttribute('class', 'sub_second');

sub.appendChild(sub_first);
sub.appendChild(sub_second);

var trans = document.createElement('div');
trans.setAttribute('class', 'trans');

var exact_trans = document.createElement('div');
exact_trans.setAttribute('class', 'exact-trans');

var exact_from = document.createElement('div');
exact_from.setAttribute('class', 'exact-from');

var span = document.createElement('span');
span.innerText = ' ' + '-' + ' ';

var exact_to = document.createElement('div');
exact_to.setAttribute('class', 'exact-to');

var trans_wrapper = document.createElement('div');
trans_wrapper.setAttribute('class', 'trans-wrapper');

var is = document.createElement('i');
is.setAttribute('class', 'fa fa-caret-right fa-2x');

var close_wrap = document.createElement('div');
close_wrap.setAttribute('class', 'close-wrap');

trans.appendChild(exact_trans);
exact_trans.appendChild(exact_from);
exact_trans.appendChild(span);
exact_trans.appendChild(exact_to);
trans.appendChild(trans_wrapper);
trans.appendChild(is);

var english_sub = document.querySelector('.english_sub');

english_sub.addEventListener('click', function () {
    english_sub.classList.toggle('sub_active');
    if (!english_sub.classList.contains('sub_active')) {
        sub_first.innerText = '';
        sub_second.innerText = '';
    }
});

setTimeout(function () {
    preloader.style.display = 'none';
}, 8000);

var swith = document.querySelectorAll('.sub_switch');
var russian_sub = document.querySelector('.russian_sub');

english_sub.addEventListener('click', function () {
    if (!english_sub.classList.contains('sub_active')) {
        english_sub.classList.add('sub_active');
        russian_sub.classList.remove('sub_active');
    } else {
        russian_sub.classList.remove('sub_active');
    }
});

russian_sub.addEventListener('click', function () {
    if (!russian_sub.classList.contains('sub_active')) {
        russian_sub.classList.add('sub_active');
        english_sub.classList.remove('sub_active');
    }
});

var _iteratorNormalCompletion = true;
var _didIteratorError = false;
var _iteratorError = undefined;

try {
    for (var _iterator = swith[Symbol.iterator](), _step; !(_iteratorNormalCompletion = (_step = _iterator.next()).done); _iteratorNormalCompletion = true) {
        i = _step.value;

        i.addEventListener('click', function () {
            sub_first.innerHTML = '';
            sub_second.innerHTML = '';

            this.classList.add('sub_active');
        });
    }

    // Копирование при клике


    // Запрашиваем перевод 
} catch (err) {
    _didIteratorError = true;
    _iteratorError = err;
} finally {
    try {
        if (!_iteratorNormalCompletion && _iterator.return) {
            _iterator.return();
        }
    } finally {
        if (_didIteratorError) {
            throw _iteratorError;
        }
    }
}

var yatr_word = {

    key: 'trnsl.1.1.20170627T100903Z.3134f17b23b61deb.2a264dc99360357e7cc00655d8e203586280a98f',
    api: 'https://translate.yandex.net/api/v1.5/tr.json/translate',

    translate: function translate(text) {
        var url = this.api + '?';

        url += 'key=' + this.key;
        url += '&text=' + text;
        url += '&lang=en-ru';

        var ajax = new XMLHttpRequest();
        ajax.open('GET', url, true);
        ajax.onreadystatechange = function () {
            if (ajax.readyState == 4) {
                if (ajax.status == 200) {
                    var translate = ajax.responseText;
                    translate = JSON.parse(translate);
                    translate = translate.text[0];
                    exact_from.innerText = text;
                    exact_to.innerText = translate;
                }
            }
        };
        ajax.send(null);
    }
};

var yatr = {

    key: 'dict.1.1.20170701T173958Z.b33374ebce561506.4d2ffc0f1cedd96f29117c84fd4d0e8145c803cd',
    api: 'https://dictionary.yandex.net/api/v1/dicservice.json/lookup',

    translate: function translate(text) {
        var url = this.api + '?';

        url += 'key=' + this.key;
        url += '&text=' + text;
        url += '&lang=en-ru';

        var ajax = new XMLHttpRequest();
        ajax.open('GET', url, true);
        ajax.onreadystatechange = function () {
            if (ajax.readyState == 4) {
                if (ajax.status == 200) {
                    var translate = ajax.responseText;
                    translate = JSON.parse(translate);
                    translate = translate.def;
                    var _iteratorNormalCompletion2 = true;
                    var _didIteratorError2 = false;
                    var _iteratorError2 = undefined;

                    try {
                        for (var _iterator2 = translate[Symbol.iterator](), _step2; !(_iteratorNormalCompletion2 = (_step2 = _iterator2.next()).done); _iteratorNormalCompletion2 = true) {
                            i = _step2.value;

                            switch (i.pos) {
                                case 'noun':
                                    var cnt_noun = 0;
                                    var _iteratorNormalCompletion3 = true;
                                    var _didIteratorError3 = false;
                                    var _iteratorError3 = undefined;

                                    try {
                                        for (var _iterator3 = i.tr[Symbol.iterator](), _step3; !(_iteratorNormalCompletion3 = (_step3 = _iterator3.next()).done); _iteratorNormalCompletion3 = true) {
                                            m = _step3.value;

                                            if (cnt_noun > 2) {
                                                break;
                                            } else {
                                                cnt_noun++;
                                                translate = m.text;
                                                if (exact_to.innerText === translate) {
                                                    break;
                                                }
                                                var noun = document.createElement('div');
                                                noun.setAttribute('class', 'word-item');
                                                noun.innerText = translate;
                                                trans_wrapper.appendChild(noun);
                                            }
                                        }
                                    } catch (err) {
                                        _didIteratorError3 = true;
                                        _iteratorError3 = err;
                                    } finally {
                                        try {
                                            if (!_iteratorNormalCompletion3 && _iterator3.return) {
                                                _iterator3.return();
                                            }
                                        } finally {
                                            if (_didIteratorError3) {
                                                throw _iteratorError3;
                                            }
                                        }
                                    }

                                    break;
                                case 'interjection':
                                    var cnt_int = 0;
                                    var _iteratorNormalCompletion4 = true;
                                    var _didIteratorError4 = false;
                                    var _iteratorError4 = undefined;

                                    try {
                                        for (var _iterator4 = i.tr[Symbol.iterator](), _step4; !(_iteratorNormalCompletion4 = (_step4 = _iterator4.next()).done); _iteratorNormalCompletion4 = true) {
                                            m = _step4.value;

                                            if (cnt_int > 2) {
                                                break;
                                            } else {
                                                cnt_int++;
                                                translate = m.text;
                                                if (exact_to.innerText === translate) {
                                                    break;
                                                }
                                                var int = document.createElement('div');
                                                int.setAttribute('class', 'word-item');
                                                int.innerText = translate;
                                                trans_wrapper.appendChild(int);
                                            }
                                        }
                                    } catch (err) {
                                        _didIteratorError4 = true;
                                        _iteratorError4 = err;
                                    } finally {
                                        try {
                                            if (!_iteratorNormalCompletion4 && _iterator4.return) {
                                                _iterator4.return();
                                            }
                                        } finally {
                                            if (_didIteratorError4) {
                                                throw _iteratorError4;
                                            }
                                        }
                                    }

                                    break;
                                case 'verb':
                                    var cnt_verb = 0;
                                    var _iteratorNormalCompletion5 = true;
                                    var _didIteratorError5 = false;
                                    var _iteratorError5 = undefined;

                                    try {
                                        for (var _iterator5 = i.tr[Symbol.iterator](), _step5; !(_iteratorNormalCompletion5 = (_step5 = _iterator5.next()).done); _iteratorNormalCompletion5 = true) {
                                            m = _step5.value;

                                            if (cnt_verb > 2) {
                                                break;
                                            } else {
                                                cnt_verb++;
                                                translate = m.text;
                                                if (exact_to.innerText === translate) {
                                                    break;
                                                }
                                                var verb = document.createElement('div');
                                                verb.setAttribute('class', 'word-item');
                                                verb.innerText = translate;
                                                trans_wrapper.appendChild(verb);
                                            }
                                        }
                                    } catch (err) {
                                        _didIteratorError5 = true;
                                        _iteratorError5 = err;
                                    } finally {
                                        try {
                                            if (!_iteratorNormalCompletion5 && _iterator5.return) {
                                                _iterator5.return();
                                            }
                                        } finally {
                                            if (_didIteratorError5) {
                                                throw _iteratorError5;
                                            }
                                        }
                                    }

                                    break;
                                case 'pronoun':
                                    var cnt_pronoun = 0;
                                    var _iteratorNormalCompletion6 = true;
                                    var _didIteratorError6 = false;
                                    var _iteratorError6 = undefined;

                                    try {
                                        for (var _iterator6 = i.tr[Symbol.iterator](), _step6; !(_iteratorNormalCompletion6 = (_step6 = _iterator6.next()).done); _iteratorNormalCompletion6 = true) {
                                            m = _step6.value;

                                            if (cnt_pronoun > 2) {
                                                break;
                                            }
                                            cnt_pronoun++;
                                            translate = m.text;
                                            if (exact_to.innerText === translate) {
                                                break;
                                            }
                                            var pronoun = document.createElement('div');
                                            pronoun.setAttribute('class', 'word-item');
                                            pronoun.innerText = translate;
                                            trans_wrapper.appendChild(pronoun);
                                        }
                                    } catch (err) {
                                        _didIteratorError6 = true;
                                        _iteratorError6 = err;
                                    } finally {
                                        try {
                                            if (!_iteratorNormalCompletion6 && _iterator6.return) {
                                                _iterator6.return();
                                            }
                                        } finally {
                                            if (_didIteratorError6) {
                                                throw _iteratorError6;
                                            }
                                        }
                                    }

                                    break;
                                case 'adjective':
                                    var cnt_adj = 0;
                                    var _iteratorNormalCompletion7 = true;
                                    var _didIteratorError7 = false;
                                    var _iteratorError7 = undefined;

                                    try {
                                        for (var _iterator7 = i.tr[Symbol.iterator](), _step7; !(_iteratorNormalCompletion7 = (_step7 = _iterator7.next()).done); _iteratorNormalCompletion7 = true) {
                                            m = _step7.value;

                                            if (cnt_adj > 2) {
                                                break;
                                            }
                                            cnt_adj++;
                                            translate = m.text;
                                            if (exact_to.innerText === translate) {
                                                break;
                                            }
                                            var adjective = document.createElement('div');
                                            adjective.setAttribute('class', 'word-item');
                                            adjective.innerText = translate;
                                            trans_wrapper.appendChild(adjective);
                                        }
                                    } catch (err) {
                                        _didIteratorError7 = true;
                                        _iteratorError7 = err;
                                    } finally {
                                        try {
                                            if (!_iteratorNormalCompletion7 && _iterator7.return) {
                                                _iterator7.return();
                                            }
                                        } finally {
                                            if (_didIteratorError7) {
                                                throw _iteratorError7;
                                            }
                                        }
                                    }

                                    break;
                                case 'adverb':
                                    var cnt_adverb = 0;
                                    var _iteratorNormalCompletion8 = true;
                                    var _didIteratorError8 = false;
                                    var _iteratorError8 = undefined;

                                    try {
                                        for (var _iterator8 = i.tr[Symbol.iterator](), _step8; !(_iteratorNormalCompletion8 = (_step8 = _iterator8.next()).done); _iteratorNormalCompletion8 = true) {
                                            m = _step8.value;

                                            if (cnt_adverb > 2) {
                                                break;
                                            }
                                            cnt_adverb++;
                                            translate = m.text;
                                            if (exact_to.innerText === translate) {
                                                break;
                                            }
                                            var adverb = document.createElement('div');
                                            adverb.setAttribute('class', 'word-item');
                                            adverb.innerText = translate;
                                            trans_wrapper.appendChild(adverb);
                                        }
                                    } catch (err) {
                                        _didIteratorError8 = true;
                                        _iteratorError8 = err;
                                    } finally {
                                        try {
                                            if (!_iteratorNormalCompletion8 && _iterator8.return) {
                                                _iterator8.return();
                                            }
                                        } finally {
                                            if (_didIteratorError8) {
                                                throw _iteratorError8;
                                            }
                                        }
                                    }

                                    break;
                                case 'preposition':
                                    var cnt_prepos = 0;
                                    var _iteratorNormalCompletion9 = true;
                                    var _didIteratorError9 = false;
                                    var _iteratorError9 = undefined;

                                    try {
                                        for (var _iterator9 = i.tr[Symbol.iterator](), _step9; !(_iteratorNormalCompletion9 = (_step9 = _iterator9.next()).done); _iteratorNormalCompletion9 = true) {
                                            m = _step9.value;

                                            if (cnt_prepos > 2) {
                                                break;
                                            }
                                            cnt_prepos++;
                                            translate = m.text;
                                            if (exact_to.innerText === translate) {
                                                break;
                                            }
                                            var prepos = document.createElement('div');
                                            prepos.setAttribute('class', 'word-item');
                                            prepos.innerText = translate;
                                            trans_wrapper.appendChild(prepos);
                                        }
                                    } catch (err) {
                                        _didIteratorError9 = true;
                                        _iteratorError9 = err;
                                    } finally {
                                        try {
                                            if (!_iteratorNormalCompletion9 && _iterator9.return) {
                                                _iterator9.return();
                                            }
                                        } finally {
                                            if (_didIteratorError9) {
                                                throw _iteratorError9;
                                            }
                                        }
                                    }

                                    break;
                                default:
                                    break;
                            }
                        }
                    } catch (err) {
                        _didIteratorError2 = true;
                        _iteratorError2 = err;
                    } finally {
                        try {
                            if (!_iteratorNormalCompletion2 && _iterator2.return) {
                                _iterator2.return();
                            }
                        } finally {
                            if (_didIteratorError2) {
                                throw _iteratorError2;
                            }
                        }
                    }

                    show_card();
                }
            }
        };
        ajax.send(null);
    }

};

// Показываем варианты

trans.addEventListener('mouseover', function () {

    var exact_from = document.querySelector('.exact-from');

    exact_trans.onclick = function () {

        var exact_to = document.querySelector('.exact-to');

        this.style.color = 'grey';
        log(exact_from.innerText, exact_to.innerText);
    };

    var word_item = document.querySelectorAll('.word-item');

    var _iteratorNormalCompletion10 = true;
    var _didIteratorError10 = false;
    var _iteratorError10 = undefined;

    try {
        for (var _iterator10 = word_item[Symbol.iterator](), _step10; !(_iteratorNormalCompletion10 = (_step10 = _iterator10.next()).done); _iteratorNormalCompletion10 = true) {
            i = _step10.value;

            i.onclick = function () {
                this.style.color = 'grey';
                log(exact_from.innerText, this.innerText);
            };
        }
    } catch (err) {
        _didIteratorError10 = true;
        _iteratorError10 = err;
    } finally {
        try {
            if (!_iteratorNormalCompletion10 && _iterator10.return) {
                _iterator10.return();
            }
        } finally {
            if (_didIteratorError10) {
                throw _iteratorError10;
            }
        }
    }
});

var cnt_dic = 0;
var all_words_in_dic = [];
var dict_wrapper_container = [];

function log(from, to) {

    var dict_from = '';

    var dict_wrapper = document.createElement('div');
    dict_wrapper.setAttribute('class', 'dict_wrapper');
    dict_wrapper.style.display = 'flex';

    var tire = document.createElement('span');
    tire.innerText = ' ' + ' - ' + ' ';

    dict_from = document.createElement('div');
    dict_from.setAttribute('class', 'dict_from');
    dict_from.innerText += from;

    var dict_to = document.createElement('div');
    dict_to.setAttribute('class', 'dict_to');
    dict_to.innerText += to;

    all_words_in_dic.push(dict_from);

    dict_wrapper.appendChild(dict_from);
    dict_wrapper.appendChild(tire);
    dict_wrapper.appendChild(dict_to);

    dict.appendChild(dict_wrapper);

    dict_wrapper_container.push(dict_wrapper);

    var cnt = '';

    cnt_dic++;

    round_cnt.innerText = cnt_dic;

    if (!round_cnt.classList.contains('lime_cnt')) {
        round_cnt.classList.add('lime_cnt');
    }
};

// Добавляем в словарь

var coordinats_left = '';
var coordinats_top = '';

sub.addEventListener('click', function (e) {
    var word = document.querySelectorAll('.word');

    trans_wrapper.innerText = " ";
    exact_trans.style.color = 'black';

    for (i = 0; i < word.length; i++) {
        word[i].onclick = function () {

            var dictation = '';
            dictation = this.innerText.toLowerCase();
            coordinats_left = '';
            coordinats_top = '';
            var mix = offset(this);
            coordinats_left = Math.round(mix.left);
            coordinats_top = Math.round(mix.top);

            yatr_word.translate(dictation);
            yatr.translate(dictation);
        };
    }
}, true);

var containers = '';

sub.addEventListener('contextmenu', function (e) {
    var word = document.querySelectorAll('.word');
    e.preventDefault();
    for (i = 0; i < word.length; i++) {

        var dictation = '';
        dictation = this.innerText.toLowerCase();

        var containers;
        word[i].oncontextmenu = function (containers) {
            containers = this.innerText;

            coordinats_left = '';
            coordinats_top = '';
            var mix = offset(this);
            coordinats_left = Math.round(mix.left);
            coordinats_top = Math.round(mix.top);

            show_card();

            if (!this.classList.contains('chosed')) {
                this.classList.add('chosed');
                num(containers);
            }
        };
    }
}, true);

function offset(el) {
    var rect = el.getBoundingClientRect(),
        scrollLeft = window.pageXOffset || document.documentElement.scrollLeft,
        scrollTop = window.pageYOffset || document.documentElement.scrollTop;
    return {
        top: rect.top + scrollTop,
        left: rect.left + scrollLeft
    };
}

var con = '';

function num(text) {
    con += text.toLowerCase() + ' ';
    yatr_word.translate(con);

    var word = document.querySelectorAll('.word');

    close_wrap.addEventListener('contextmenu', function (e) {
        e.preventDefault();
        this.classList.remove('show');
        trans.classList.remove('show');
        con = '';
        var _iteratorNormalCompletion11 = true;
        var _didIteratorError11 = false;
        var _iteratorError11 = undefined;

        try {
            for (var _iterator11 = word[Symbol.iterator](), _step11; !(_iteratorNormalCompletion11 = (_step11 = _iterator11.next()).done); _iteratorNormalCompletion11 = true) {
                i = _step11.value;

                i.classList.remove('chosed');
                if (!exact_trans) {
                    var _iteratorNormalCompletion12 = true;
                    var _didIteratorError12 = false;
                    var _iteratorError12 = undefined;

                    try {
                        for (var _iterator12 = exact_trans[Symbol.iterator](), _step12; !(_iteratorNormalCompletion12 = (_step12 = _iterator12.next()).done); _iteratorNormalCompletion12 = true) {
                            m = _step12.value;

                            m.style.color = 'black';
                        }
                    } catch (err) {
                        _didIteratorError12 = true;
                        _iteratorError12 = err;
                    } finally {
                        try {
                            if (!_iteratorNormalCompletion12 && _iterator12.return) {
                                _iterator12.return();
                            }
                        } finally {
                            if (_didIteratorError12) {
                                throw _iteratorError12;
                            }
                        }
                    }
                }
            }
        } catch (err) {
            _didIteratorError11 = true;
            _iteratorError11 = err;
        } finally {
            try {
                if (!_iteratorNormalCompletion11 && _iterator11.return) {
                    _iterator11.return();
                }
            } finally {
                if (_didIteratorError11) {
                    throw _iteratorError11;
                }
            }
        }
    });

    close_wrap.addEventListener('click', function (e) {
        con = '';
        var _iteratorNormalCompletion13 = true;
        var _didIteratorError13 = false;
        var _iteratorError13 = undefined;

        try {
            for (var _iterator13 = word[Symbol.iterator](), _step13; !(_iteratorNormalCompletion13 = (_step13 = _iterator13.next()).done); _iteratorNormalCompletion13 = true) {
                i = _step13.value;

                i.classList.remove('chosed');
                if (!exact_trans) {
                    var _iteratorNormalCompletion14 = true;
                    var _didIteratorError14 = false;
                    var _iteratorError14 = undefined;

                    try {
                        for (var _iterator14 = exact_trans[Symbol.iterator](), _step14; !(_iteratorNormalCompletion14 = (_step14 = _iterator14.next()).done); _iteratorNormalCompletion14 = true) {
                            m = _step14.value;

                            m.style.color = 'black';
                        }
                    } catch (err) {
                        _didIteratorError14 = true;
                        _iteratorError14 = err;
                    } finally {
                        try {
                            if (!_iteratorNormalCompletion14 && _iterator14.return) {
                                _iterator14.return();
                            }
                        } finally {
                            if (_didIteratorError14) {
                                throw _iteratorError14;
                            }
                        }
                    }
                }
            }
        } catch (err) {
            _didIteratorError13 = true;
            _iteratorError13 = err;
        } finally {
            try {
                if (!_iteratorNormalCompletion13 && _iterator13.return) {
                    _iterator13.return();
                }
            } finally {
                if (_didIteratorError13) {
                    throw _iteratorError13;
                }
            }
        }
    });
}

function show_card() {

    trans.classList.add('show');
    close_wrap.classList.add('show');

    var trans_height = trans.offsetHeight;

    trans.style.left = coordinats_left - 35 + 'px';
    trans.style.top = coordinats_top - (trans_height + 48) + 'px';
}

close_wrap.addEventListener('click', function () {

    this.classList.remove('show');
    trans.classList.remove('show');
    trans_wrapper.innerText = " ";
});

// Как использовать

var use_span = document.querySelectorAll('.use_span');
var use_now = document.querySelector('.use_now');
var use_late = document.querySelector('.use_late');
if (use_late) {
    use_late.innerHTML = use_span.length;
}

var use_span__cnt = 1;

function how_to_use__right(e) {

    if (use_span__cnt == use_span.length) {
        return false;
    } else {
        use_span[use_span__cnt - 1].classList.remove('use_span__visible');
    }

    use_span__cnt++;
    use_now.innerHTML = use_span__cnt;

    use_span[use_span__cnt - 1].classList.add('use_span__visible');
}

function how_to_use__left() {

    if (use_span__cnt == 1) {
        return false;
    }

    use_span[use_span__cnt - 1].classList.remove('use_span__visible');

    use_span__cnt--;
    use_now.innerHTML = use_span__cnt;

    use_span[use_span__cnt - 1].classList.add('use_span__visible');
}

var preloader = document.querySelector('.preloader');