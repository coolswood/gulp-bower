var gulp = require('gulp'),
    postcss = require('gulp-postcss'),
    autoprefixer = require('autoprefixer'),
    focus = require('postcss-focus'), // Добавляет focus к hover
    size = require('postcss-size'), // Задавать сразу высоту и ширину
    clearfix = require('postcss-clearfix'), // clear: fix;
    responsive_img = require('postcss-responsive-images'), // image-size: responsive;
    nopix = require('postcss-default-unit'), // Добавляет px
    center = require('postcss-center'), // Центрировать изображение
    longhand = require('postcss-merge-longhand'), // Комбинирует margin, border, padding
    assets = require('postcss-assets'), // Находит путь к изображениям, инлайнит 'resolve', 'inline'
    font = require('postcss-font-magician'), // Автоматическое подключение шрифтов
    sprite = require('gulp.spritesmith'), // Делает спрайты
    responsive = require('postcss-responsive-font'), // Делаем адаптивный текст
    inline = require('postcss-inline-media'), // Инлайнит медиа запросы
    z_index = require('postcss-zindex'), // Приводит к нормальным числам z-index
    duble_css = require('postcss-discard-duplicates'), // Убирает дубли стилей
    watch = require('gulp-watch'),
    gcmq = require('gulp-group-css-media-queries'), // Группирует медиа запросы +
    gzip = require('gulp-gzip'), // Архивирует
    cssnano = require('gulp-cssnano'), // Сжимаем сss
    styl = require('gulp-stylus'),
    browserSync = require('browser-sync'),
    uncss = require('gulp-uncss'), // убираем неиспользуемые стили
    rev = require('gulp-rev-append'), // Убиваем кэш
    smartgrid = require('smart-grid'), // Сетка
    concat = require('gulp-concat'), // Собираем скрипты
    shorthand = require('gulp-shorthand'), // Причесываем css
    babel = require('gulp-babel');
    reload = browserSync.reload;

var path = {
    build: { //Тут мы укажем куда складывать готовые после сборки файлы
        html: '../train/build/',
        js: '../train/build/js/',
        css: '../train/build/css/',
        img: '../train/build/img/',
        fonts: '../train/build/fonts/'
    },
    src: { //Пути откуда брать исходники
        jade: '../train/src/*.html', //Синтаксис src/*.html говорит gulp что мы хотим взять все файлы с расширением .html
        js: '../train/src/js/*.js',
        style: '../train/src/style/*.styl',
        img: '../train/src/img/**/*.*',
        fonts: '../train/src/fonts/**/*.*'
    },
    watch: { //Тут мы укажем, за изменением каких файлов мы хотим наблюдать
        jade: '../train/src/**/*.html',
        js: '../train/src/js/**/*.js',
        style: '../train/src/style/**/*.styl',
        img: '../train/src/img/*.*',
        fonts: '../train/src/fonts/*.*'
    },
};

var config = {
    server: {
        baseDir: "../train/build"
    },
    tunnel: true,
    host: 'localhost',
    port: 9000,
    logPrefix: "Frontend_Devil"
};

var processors = [
     
   inline,
   autoprefixer({browsers: ['last 2 version']}),
   focus,
   size,
   longhand,
   clearfix,
   responsive,
   z_index,
   duble_css,
   nopix,
   center,
   font,
   responsive_img
      
 ];

gulp.task('html:build', function () {
    gulp.src(path.src.jade) //Выберем файлы по нужному пути
        
        .pipe(gulp.dest(path.build.html)) //Выплюнем их в папку build
        .pipe(reload({stream: true})); //И перезагрузим наш сервер для обновлений
});

gulp.task('js:build', function () {
    gulp.src(path.src.js) //Выберем файлы по нужному пути
        .pipe(gulp.dest(path.build.js)) //Выплюнем их в папку build
        .pipe(reload({stream: true})); //И перезагрузим наш сервер для обновлений
});

gulp.task('image:build', function () {
    gulp.src(path.src.img)
        .pipe(gulp.dest(path.build.img)) 
        .pipe(reload({stream: true}));
});

gulp.task('fonts:build', function () {
    gulp.src(path.src.fonts) 
        .pipe(gulp.dest(path.build.fonts)) 
        .pipe(reload({stream: true}));
});

gulp.task('style:build', function () {
    gulp.src(path.src.style) 
        .pipe(styl()) 
        .pipe(postcss(processors))
        .pipe(postcss([assets({basePath: '../train/build', relative: 'css/', loadPaths: ['img/**/']})]))
        .pipe(shorthand())
        .pipe(gcmq())
        .pipe(gulp.dest(path.build.css)) //И в build
        .pipe(reload({stream: true}));
});


gulp.task('rev', function() {
  gulp.src('build/index.html')
    .pipe(rev()) //?rev=@@hash
    .pipe(gulp.dest('build/'));
});


gulp.task('sprite', function() {
    var spriteData = 
        gulp.src('./../train/src/img/sprite/*.*') // путь, откуда берем картинки для спрайта
            .pipe(sprite({
                imgName: 'sprite.png',
                cssName: 'sprite.css',
            }));

    spriteData.img.pipe(gulp.dest('./../train/build/img/sprite/')); // путь, куда сохраняем картинку
    spriteData.css.pipe(gulp.dest('./../train/src/style')); // путь, куда сохраняем стили
});
 
/* It's principal settings in smart grid project */
var settings = {
    outputStyle: 'styl', /* less || scss || sass || styl */
    columns: 4, /* number of grid columns */
    offset: '20px', /* gutter width px || % */
    mobileFirst: false, /* mobileFirst ? 'min-width' : 'max-width' */
    container: {
        maxWidth: '1200px', /* max-width оn very large screen */
        fields: '30px' /* side fields */
    },
    breakPoints: {
        lg: {
            width: '1100px', /* -> @media (max-width: 1100px) */
        },
        md: {
            width: '960px'
        },
        sm: {
            width: '780px',
            fields: '15px' /* set fields only if you want to change container.fields */
        },
        xs: {
            width: '560px'
        }
        /* 
        We can create any quantity of break points.
 
        some_name: {
            width: 'Npx',
            fields: 'N(px|%|rem)',
            offset: 'N(px|%|rem)'
        }
        */
    }
};
 
smartgrid('../train/src/style/instrum', settings);

gulp.task('build', [
    'html:build',
    'js:build',
    'style:build',
    'image:build',
    'fonts:build',
    'rev'
]);


gulp.task('watch', function(){
    watch([path.watch.jade], function(event, cb) {
        gulp.start('html:build');
    });
    watch([path.watch.style], function(event, cb) {
        gulp.start('style:build');
    });
    watch([path.watch.style], function(event, cb) {
        gulp.start('js:build');
    });
    watch([path.watch.img], function(event, cb) {
        gulp.start('image:build');
    });
    watch([path.watch.fonts], function(event, cb) {
        gulp.start('fonts:build');
    });
    watch([path.watch.style], function(event, cb) {
        gulp.start('rev');
    });
});

gulp.task('webserver', function () {
    browserSync(config);
});


gulp.task('default', ['build', 'webserver', 'watch']);



gulp.task('prodaction', function () {
    return gulp.src('../train/build/css/style.css')
        .pipe(uncss({
            html: ['../train/build/index.html']
        }))
        .pipe(cssnano())
        //.pipe(gzip())
        .pipe(gulp.dest('../train/prodaction/css/'));
});

gulp.task('bab', () => {
    return gulp.src('babel/src/*.js')
   .pipe(babel({
            presets: ['es2015']
        }))
        .pipe(gulp.dest('babel/build/'));
});

gulp.task('concat', [
   
   'concat:js',
   'concat:css'
])

gulp.task('concat:js', function() {
  return gulp.src([
     
     './bower_components/jquery/dist/jquery.slim.min.js',
     './bower_components/owl.carousel/dist/owl.carousel.min.js',
     './bower_components/custom-scroll/jquery.custom-scroll.min.js'
     //'./bower_components/jQuery-EasyTabs/lib/jquery.easytabs.min.js',
     //'./bower_components/magnific-popup/dist/jquery.magnific-popup.min.js',
     //'./bower_components/parallax.js/parallax.min.js',
     //'./bower_components/jquery-validation/dist/jquery.validate.min.js'
     
     
     ])
    .pipe(concat('plagins.js'))
    .pipe(gulp.dest('../train/src/js'));
});

gulp.task('concat:css', function() {
  return gulp.src([
     
     './bower_components/font-awesome/css/font-awesome.css',
     //'./bower_components/magnific-popup/dist/magnific-popup.css',
     './bower_components/owl.carousel/dist/assets/owl.carousel.min.css',
     './bower_components/owl.carousel/dist/assets/owl.theme.default.min.css',
     './bower_components/custom-scroll/jquery.custom-scroll.css'
     
     
     ])
    .pipe(concat('plagins.styl'))
    .pipe(gulp.dest('../train/src/style'));
});